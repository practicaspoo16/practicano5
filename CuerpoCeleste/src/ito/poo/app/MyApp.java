package ito.poo.app;

import ito.poo.clases.Ubicacion;
import ito.poo.clases.CuerpoCeleste;

public class MyApp {
	
	static void run() {
		Ubicacion u= new Ubicacion(90,75,"Mayo",9500);
		
		System.out.println(u);
		
		CuerpoCeleste c= new CuerpoCeleste("Edens Zero","Gas");
		
		System.out.println(c);
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
        run();
	}

}
