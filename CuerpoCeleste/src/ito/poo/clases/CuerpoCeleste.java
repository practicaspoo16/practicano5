package ito.poo.clases;

import java.util.ArrayList;


public class CuerpoCeleste {
	
	private String nombre;
	private ArrayList<Ubicacion> localizaciones;
	private String composicion;
	
	public CuerpoCeleste(String nombre, String composicion) {
		super();
		this.nombre = nombre;
		this.localizaciones= new ArrayList<Ubicacion>();
		this.composicion = composicion;
	}
	
	public float desplazamiento(int i, int j) {
		return 0f;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Ubicacion> getLocalizaciones() {
		return localizaciones;
	}

	public void setLocalizaciones(ArrayList<Ubicacion> localizaciones) {
		this.localizaciones = localizaciones;
	}

	public String getComposicion() {
		return composicion;
	}

	public void setComposicion(String composicion) {
		this.composicion = composicion;
	}

	@Override
	public String toString() {
		return "CuerpoCeleste [nombre=" + nombre + ", localizaciones=" + localizaciones + ", composicion=" + composicion
				+ "]";
	}
	
	

}
