package ito.poo.clases;


public class Periodo {
	
	private String tiempoCosecha;
	private float cantCosechaxtiempo;
	
	public Periodo(String tiempoCosecha, float cantCosechaxtiempo) {
		super();
		this.tiempoCosecha = tiempoCosecha;
		this.cantCosechaxtiempo = cantCosechaxtiempo;
	}
	
	public float costoPeriodo(float costoProm) {
		return this.cantCosechaxtiempo*costoProm;
	}
	
	public float gananciaEstimada(float costoProm) {
		return costoProm*cantCosechaxtiempo;
	}

	public String getTiempoCosecha() {
		return tiempoCosecha;
	}

	public void setTiempoCosecha(String tiempoCosecha) {
		this.tiempoCosecha = tiempoCosecha;
	}

	public float getCantCosechaxtiempo() {
		return cantCosechaxtiempo;
	}

	public void setCantCosechaxtiempo(float cantCosechaxtiempo) {
		this.cantCosechaxtiempo = cantCosechaxtiempo;
	}

	@Override
	public String toString() {
		return "Periodo [tiempoCosecha=" + tiempoCosecha + ", cantCosechaxtiempo=" + cantCosechaxtiempo + "]";
	}
	
	

}
