package ito.poo.clases;

import java.util.ArrayList;

public class Fruta {
	
	private String nombre;
	private float extension;
	private float costoPromedio;
	private float precioVentaProm;
	private ArrayList<Periodo> periodos;
	
	public Fruta(String nombre, float extension, float costoPromedio, float precioVentaProm) {
		super();
		this.nombre = nombre;
		this.extension = extension;
		this.costoPromedio = costoPromedio;
		this.precioVentaProm = precioVentaProm;
		this.periodos= new ArrayList<Periodo>();
	}
	
	public void agregarPeriodo(Periodo p) {
		periodos.add(p);	
	}
	
	public Periodo recuperaLote(int i) {
		Periodo l= null;
		if(this.periodos.size()>i)
			l=this.periodos.get(i);
		return l;
	}
	
	public boolean eliminarPeriodo(int i) {
		boolean test=false;
		periodos.remove((int) i);
		test=true;
		return test;
	}
	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getExtension() {
		return extension;
	}

	public void setExtension(float extension) {
		this.extension = extension;
	}

	public float getCostoPromedio() {
		return costoPromedio;
	}

	public void setCostoPromedio(float costoPromedio) {
		this.costoPromedio = costoPromedio;
	}

	public float getPrecioVentaProm() {
		return precioVentaProm;
	}

	public void setPrecioVentaProm(float precioVentaProm) {
		this.precioVentaProm = precioVentaProm;
	}

	public ArrayList<Periodo> getPeriodos() {
		return periodos;
	}

	public void setPeriodos(ArrayList<Periodo> periodos) {
		this.periodos = periodos;
	}

	@Override
	public String toString() {
		return "Fruta [nombre=" + nombre + ", extension=" + extension + ", costoPromedio=" + costoPromedio
				+ ", precioVentaProm=" + precioVentaProm + ", periodos=" + periodos + "]";
	}
	
	

}
