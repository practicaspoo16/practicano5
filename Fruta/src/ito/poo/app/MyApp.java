package ito.poo.app;

import ito.poo.clases.Fruta;
import ito.poo.clases.Periodo;

public class MyApp {
	
	static void run() {
		Fruta f= new Fruta("Manzana", 10, 100, 250);
		
		System.out.println(f);
		f.agregarPeriodo(new Periodo("Verano",5));
		System.out.println(f);
		f.agregarPeriodo(new Periodo("Oto�o",3));
		System.out.println(f);
		f.agregarPeriodo(new Periodo("Primavera",2));
		System.out.println(f);
		f.agregarPeriodo(new Periodo("Oto�o",4));
		System.out.println(f);
		System.out.println(f.eliminarPeriodo(0));
		System.out.println(f);
		
		System.out.println("Costo del periodo="+f.recuperaLote(1).costoPeriodo(f.getCostoPromedio()));
		System.out.println("Ganancia estimada"+f.recuperaLote(1).gananciaEstimada(f.getPrecioVentaProm()));
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
        run();
	}

}
