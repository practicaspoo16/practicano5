package ito.poo.claases;

import java.time.LocalDate;

public class Lote {
	
	private int numLote;
	private int numPiezas;
	private LocalDate fecha;
	
	public Lote(int numLote, int numPiezas, LocalDate fecha) {
		super();
		this.numLote = numLote;
		this.numPiezas = numPiezas;
		this.fecha = fecha;
	}
	
	public float costoProduccion(float costoPieza) {
		return this.numPiezas*costoPieza;
	}
	
	
	public float montoRecuperacionPorLote(float costoPieza) {
		return this.costoProduccion(costoPieza)*0.05f;
	}
	
	public float montoRecuperacionPorPieza(float costoPieza) {
		return costoPieza*0.15f;
	}

	public int getNumLote() {
		return numLote;
	}

	public void setNumLote(int numLote) {
		this.numLote = numLote;
	}

	public int getNumPiezas() {
		return numPiezas;
	}

	public void setNumPiezas(int numPiezas) {
		this.numPiezas = numPiezas;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	@Override
	public String toString() {
		return "Lote [numLote=" + numLote + ", numPiezas=" + numPiezas + ", fecha=" + fecha + "]";
	}
	
}
