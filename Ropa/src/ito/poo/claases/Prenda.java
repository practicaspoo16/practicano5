package ito.poo.claases;

import java.util.ArrayList;

public class Prenda {
	
	
	private int modelo;
	private String tela;
	private float costoProduccion;
	private String genero;
	private String temporada;
	private ArrayList<Lote> lotes;
	
	public Prenda(int modelo, String tela, float costoProduccion, String genero, String temporada) {
		super();
		this.modelo = modelo;
		this.tela = tela;
		this.costoProduccion = costoProduccion;
		this.genero = genero;
		this.temporada = temporada;
		this.lotes= new ArrayList<Lote>();
	}

	public void addNvoLote(Lote lote) {
	      lotes.add(lote);	
	}
	
	public Lote recuperaLote(int i) {
		Lote l= null;
		if(this.lotes.size()>i)
			l=this.lotes.get(i);
		return l;
	}
	
	public int getModelo() {
		return modelo;
	}

	public void setModelo(int modelo) {
		this.modelo = modelo;
	}

	public String getTela() {
		return tela;
	}

	public void setTela(String tela) {
		this.tela = tela;
	}

	public float getCostoProduccion() {
		return costoProduccion;
	}

	public void setCostoProduccion(float costoProduccion) {
		this.costoProduccion = costoProduccion;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getTemporada() {
		return temporada;
	}

	public void setTemporada(String temporada) {
		this.temporada = temporada;
	}

	public ArrayList<Lote> getLotes() {
		return lotes;
	}

	@Override
	public String toString() {
		return "Prenda [modelo=" + modelo + ", tela=" + tela + ", costoProduccion=" + costoProduccion + ", genero="
				+ genero + ", temporada=" + temporada + ", lotes=" + lotes + "]";
	}

	
}
