package ito.poo.app;

import ito.poo.clases.Calzado;

public class MyApp {

	static void run() {
		Calzado c= new Calzado(73,"Tela",50);
		
		System.out.println(c);
		c.agregaColor("negro");
		c.agregaColor("cafe");
		c.agregaColor("azul");
		c.agregaColor("cafe");
		System.out.println(c);
		System.out.println(c.costoXLote(54.4f));
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
        run();
	}

}
