package ito.poo.clases;

import java.util.ArrayList;

public class Calzado {

		
		private int clave;
		private String material;
		private int cantidadProducida;
		private ArrayList<String> colores;
		
		public Calzado(int clave, String material, int cantidadProducida) {
			super();
			this.clave = clave;
			this.material = material;
			this.cantidadProducida = cantidadProducida;
			this.colores= new ArrayList<String>();
		}
		
		 public boolean agregaColor(String color) {
			boolean test=false;
			if(!this.colores.contains(color)) {
			      this.colores.add(color);
			      test=true;
			}
			return test;
		}
		
		public float costoXLote(float costoUnidad) {
			return this.cantidadProducida*costoUnidad;
		}

		public int getClave() {
			return clave;
		}

		public void setClave(int clave) {
			this.clave = clave;
		}

		public String getMaterial() {
			return material;
		}

		public void setMaterial(String material) {
			this.material = material;
		}

		public int getCantidadProducida() {
			return cantidadProducida;
		}

		public void setCantidadProducida(int cantidadProducida) {
			this.cantidadProducida = cantidadProducida;
		}

		public ArrayList<String> getColores() {
			return colores;
		}

		@Override
		public String toString() {
			return "Calzado [clave=" + clave + ", material=" + material + ", cantidadProducida=" + cantidadProducida
					+ ", colores=" + colores + "]";
		}
		
		
}